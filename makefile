INPUTFILES = diss.tex           \
             abbreviations.tex  \
             eidestattliche_versicherung.tex\
             prepublications.tex\
             abstract.tex       \
             zusammenfassung.tex\
	         kap_01.tex         \
	         kap_02.tex         \
	         kap_03.tex         \
	         kap_04.tex         \
	         anhang.tex         \
             bibliographie.tex  \
	         danksagung.tex     \
             lebenslauf.tex     \

all: 	diss.pdf

diss.pdf:$(INPUTFILES)
	pdflatex diss
	makeglossaries diss
	bibtex diss
	pdflatex diss
	pdflatex diss

abbreviations.tex: abbreviations.txt
	python make_abbreviations.py abbreviations.txt abbreviations.tex

.phony: clean
clean:	
	rm -rf *~ *.aux *.log *.toc *.blg *.bbl *.lof *lot *.out *.dvi *.acn *.acr *.alg *.gls *.glo *.glsdef *.ist *.bcf *.glg

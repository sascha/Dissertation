\contentsline {chapter}{Publications and Declaration of Co-Authorship}{15}
\contentsline {subsubsection}{Chapter 2}{15}
\contentsline {subsubsection}{Chapter 3}{15}
\contentsline {subsubsection}{Chapter 4}{16}
\contentsline {subsubsection}{Chapter 5}{16}
\contentsline {subsubsection}{Chapter 6}{16}
\contentsline {chapter}{\numberline {1}Introduction}{19}
\contentsline {section}{\numberline {1.1}Unsupervised Training Methods}{20}
\contentsline {subsection}{\numberline {1.1.1}Continuous Bag of Words}{20}
\contentsline {subsection}{\numberline {1.1.2}Skip-Gram}{22}
\contentsline {subsection}{\numberline {1.1.3}Global Vectors}{23}
\contentsline {section}{\numberline {1.2}Graph-Based Word Embeddings}{23}
\contentsline {subsection}{\numberline {1.2.1}PageRank}{24}
\contentsline {subsection}{\numberline {1.2.2}Personalized PageRank}{25}
\contentsline {subsection}{\numberline {1.2.3}SimRank}{26}
\contentsline {section}{\numberline {1.3}Intrinsic Evaluation}{27}
\contentsline {subsection}{\numberline {1.3.1}Word Similarity}{27}
\contentsline {subsection}{\numberline {1.3.2}Word Analogy}{28}
\contentsline {section}{\numberline {1.4}Task Specific Embeddings}{30}
\contentsline {subsection}{\numberline {1.4.1}Word Embeddings}{32}
\contentsline {subsection}{\numberline {1.4.2}Word-level Embeddings}{32}
\contentsline {subsection}{\numberline {1.4.3}Sentence Embeddings}{33}
\contentsline {subsection}{\numberline {1.4.4}Document Embeddings}{34}
\contentsline {chapter}{\numberline {2}CoSimRank}{37}
\contentsline {section}{\numberline {2.1}\ignorespaces Introduction}{38}
\contentsline {section}{\numberline {2.2}\ignorespaces Related Work}{38}
\contentsline {section}{\numberline {2.3}\ignorespaces CoSimRank}{40}
\contentsline {subsection}{\numberline {2.3.1}\ignorespaces Personalized PageRank}{40}
\contentsline {subsection}{\numberline {2.3.2}\ignorespaces Similarity of Vectors}{40}
\contentsline {subsection}{\numberline {2.3.3}\ignorespaces Matrix Formulation}{41}
\contentsline {subsection}{\numberline {2.3.4}\ignorespaces Convergence Properties}{41}
\contentsline {section}{\numberline {2.4}\ignorespaces Comparison to SimRank}{41}
\contentsline {section}{\numberline {2.5}\ignorespaces Extensions}{42}
\contentsline {subsection}{\numberline {2.5.1}\ignorespaces Weighted Edges}{42}
\contentsline {subsection}{\numberline {2.5.2}\ignorespaces CoSimRank Across Graphs}{42}
\contentsline {subsection}{\numberline {2.5.3}\ignorespaces Typed Edges}{42}
\contentsline {subsection}{\numberline {2.5.4}\ignorespaces Similarity of Sets of Nodes}{43}
\contentsline {section}{\numberline {2.6}\ignorespaces Experiments}{43}
\contentsline {subsection}{\numberline {2.6.1}\ignorespaces Baselines}{43}
\contentsline {subsection}{\numberline {2.6.2}\ignorespaces Synonym Extraction}{43}
\contentsline {subsection}{\numberline {2.6.3}\ignorespaces Lexicon Extraction}{44}
\contentsline {subsection}{\numberline {2.6.4}\ignorespaces Run Time Performance}{45}
\contentsline {subsection}{\numberline {2.6.5}\ignorespaces Comparison with WINTIAN}{45}
\contentsline {subsection}{\numberline {2.6.6}\ignorespaces Error Analysis}{46}
\contentsline {section}{\numberline {2.7}\ignorespaces Summary}{46}
\contentsline {chapter}{\numberline {3}AutoExtend for Synsets and Lexemes}{49}
\contentsline {section}{\numberline {3.1}\ignorespaces Introduction}{50}
\contentsline {section}{\numberline {3.2}\ignorespaces Model}{51}
\contentsline {subsection}{\numberline {3.2.1}\ignorespaces Learning}{52}
\contentsline {subsection}{\numberline {3.2.2}\ignorespaces Matrix Formalization}{53}
\contentsline {subsection}{\numberline {3.2.3}\ignorespaces Lexeme Embeddings}{53}
\contentsline {subsection}{\numberline {3.2.4}\ignorespaces WordNet Relations}{53}
\contentsline {subsection}{\numberline {3.2.5}\ignorespaces Implementation}{53}
\contentsline {subsection}{\numberline {3.2.6}\ignorespaces Column Normalization}{54}
\contentsline {section}{\numberline {3.3}\ignorespaces Data\tmspace +\thinmuskip {.1667em} Experiments and Evaluation}{54}
\contentsline {subsection}{\numberline {3.3.1}\ignorespaces Word Sense Disambiguation}{54}
\contentsline {subsection}{\numberline {3.3.2}\ignorespaces Synset and Lexeme Similarity}{56}
\contentsline {section}{\numberline {3.4}\ignorespaces Analysis}{56}
\contentsline {section}{\numberline {3.5}\ignorespaces Resources other than WordNet}{57}
\contentsline {section}{\numberline {3.6}\ignorespaces Related Work}{57}
\contentsline {section}{\numberline {3.7}\ignorespaces Conclusion}{58}
\contentsline {chapter}{\numberline {4}Ultradense Word Embeddings}{61}
\contentsline {section}{\numberline {4.1}\ignorespaces Introduction}{62}
\contentsline {section}{\numberline {4.2}\ignorespaces Model}{63}
\contentsline {subsection}{\numberline {4.2.1}\ignorespaces Separating Words of Different Groups}{63}
\contentsline {subsection}{\numberline {4.2.2}\ignorespaces Aligning Words of the Same Group}{63}
\contentsline {subsection}{\numberline {4.2.3}\ignorespaces Training}{64}
\contentsline {subsection}{\numberline {4.2.4}\ignorespaces Orthogonalization}{64}
\contentsline {section}{\numberline {4.3}\ignorespaces Lexicon Creation}{64}
\contentsline {section}{\numberline {4.4}\ignorespaces Evaluation}{65}
\contentsline {subsection}{\numberline {4.4.1}\ignorespaces Top-Ranked Words}{65}
\contentsline {subsection}{\numberline {4.4.2}\ignorespaces Quality of Predictions}{66}
\contentsline {subsection}{\numberline {4.4.3}\ignorespaces Determining Association Strength}{66}
\contentsline {subsection}{\numberline {4.4.4}\ignorespaces Text Polarity Classification}{67}
\contentsline {section}{\numberline {4.5}\ignorespaces Parameter Analysis}{68}
\contentsline {subsection}{\numberline {4.5.1}\ignorespaces Size of Subspace}{68}
\contentsline {subsection}{\numberline {4.5.2}\ignorespaces Size of Training Resource}{69}
\contentsline {section}{\numberline {4.6}\ignorespaces Related Work}{69}
\contentsline {section}{\numberline {4.7}\ignorespaces Conclusion}{70}
\contentsline {chapter}{\numberline {5}Word Embedding Calculus}{73}
\contentsline {section}{\numberline {5.1}\ignorespaces Introduction}{74}
\contentsline {section}{\numberline {5.2}\ignorespaces Word Embedding Transformation}{74}
\contentsline {section}{\numberline {5.3}\ignorespaces Setup and Method}{75}
\contentsline {section}{\numberline {5.4}\ignorespaces Evaluation}{76}
\contentsline {subsection}{\numberline {5.4.1}\ignorespaces Antonym Classification}{76}
\contentsline {subsection}{\numberline {5.4.2}\ignorespaces Polarity Spectrum Creation}{76}
\contentsline {subsection}{\numberline {5.4.3}\ignorespaces Morphological Analogy}{77}
\contentsline {subsection}{\numberline {5.4.4}\ignorespaces POS Tagging}{78}
\contentsline {section}{\numberline {5.5}\ignorespaces Related Work}{78}
\contentsline {section}{\numberline {5.6}\ignorespaces Conclusion}{78}
\contentsline {chapter}{\numberline {6}AutoExtend with Semantic Resources}{81}
\contentsline {section}{\numberline {6.1}\ignorespaces Introduction}{82}
\contentsline {section}{\numberline {6.2}\ignorespaces Model}{83}
\contentsline {subsection}{\numberline {6.2.1}\ignorespaces General Framework}{84}
\contentsline {subsection}{\numberline {6.2.2}\ignorespaces Additive Edges}{86}
\contentsline {subsection}{\numberline {6.2.3}\ignorespaces Learning Through Autoencoding}{88}
\contentsline {subsection}{\numberline {6.2.4}\ignorespaces Matrix Formulation}{89}
\contentsline {subsection}{\numberline {6.2.5}\ignorespaces Lexeme Embeddings}{90}
\contentsline {subsection}{\numberline {6.2.6}\ignorespaces Similarity Edges}{90}
\contentsline {subsection}{\numberline {6.2.7}\ignorespaces Column Normalization}{91}
\contentsline {subsection}{\numberline {6.2.8}\ignorespaces Implementation}{91}
\contentsline {section}{\numberline {6.3}\ignorespaces Data}{91}
\contentsline {subsection}{\numberline {6.3.1}\ignorespaces WordNet}{92}
\contentsline {subsection}{\numberline {6.3.2}\ignorespaces GermaNet}{92}
\contentsline {subsection}{\numberline {6.3.3}\ignorespaces Freebase}{93}
\contentsline {section}{\numberline {6.4}\ignorespaces Experiments and Evaluation}{94}
\contentsline {subsection}{\numberline {6.4.1}\ignorespaces Word Sense Disambiguation}{94}
\contentsline {subsection}{\numberline {6.4.2}\ignorespaces Entity Linking}{96}
\contentsline {subsection}{\numberline {6.4.3}\ignorespaces Word-in-Context Similarity}{96}
\contentsline {subsection}{\numberline {6.4.4}\ignorespaces Word Similarity}{97}
\contentsline {subsection}{\numberline {6.4.5}\ignorespaces Synset Alignment}{98}
\contentsline {subsection}{\numberline {6.4.6}\ignorespaces Analysis}{99}
\contentsline {section}{\numberline {6.5}\ignorespaces Related Work}{100}
\contentsline {section}{\numberline {6.6}\ignorespaces Conclusion}{102}
\contentsline {chapter}{Bibliography}{109}
\contentsline {chapter}{Curriculum Vitae}{123}

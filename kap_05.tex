\chapter{Task Specific Embeddings}
%
\section{Introduction}
% overview 
In the previous chapters we learned word embeddings on unlabeled data. These embeddings where thought to be general and not specific to any task. The idea of these embeddings is that unlabeled corpora is widely available and the information contained in them can be transferred to task where less training data is available. However there are also tasks where we do have a lot of training data, e.g. machine translation for popular languages or questions answering. For these application we can also learn task specific embeddings on the fly while training an end-to-end system.

In this chapter we will concentrate on word, word-level, sentence and document embeddings for machine comprehension. A word-level embeddings corresponds to a word but is not unique for all occurrences of this word. Depending on the architecture we are using a word-level embedding also depends on the right or left context or both. We use rc-data \cite{nips15hermann} and SQuAD \cite{RajpurkarZLL16} as datasets. Both have a collection of document, question and answer triples. The question is thought to be solvable by reading the document and without using outside knowledge, excluding language knowledge.

In the basic setup we will use an \LSTM to learn the word and word-level embeddings. We use a sentence embedding to encode the question, whereas the sentence embedding is simply the word-level embedding of the first word concatenated with the word-level embedding of the last word. The word-level embeddings depend on the right, respectively left context. To encode the document we use an attention mechanism over all word-level embeddings of the document. The attention mechanism depends on the question encoding, which means that it is not a general encoding of the entire document but only of the part that is relevant to answer the question. We will explore additional setups throughout the chapter.

\section{Model}
The base model is the Attentive Reader described by \newcite{nips15hermann}, which in turn is a simplified version of the by encoder-decoder with alignment mechanism by \newcite{bahdanau2014neural}. In this context alignment and attention are in interchangeable. The model is simplified in terms that the Attentive Reader only has to predict one token instead of an entire sequence. Instead of using the output sequence for the alignment (i.e. attention) mechanism we use the question embedding. However with this setup we can only solve the rc-data and not the SQaAD dataset. Thus one modification we did to the Attentive Reader is to take the question as well as the generated output as an input to the attention mechanism. This allows us to generate a sequence as output, e.g. a short phrase to answer the question. All other modifications are applicable to the base and this extended version.
%
\subsection{Bidirectional \LSTM}
We use an \LSTM implementation by \newcite{Graves13}. This architecture is based on the work by \newcite{hochreiter1997long} which in turn is modified version of an \RNN. The implementation is based on several functions.
\begin{align}
 i_t &= \sigma(W_{xi} x_t + W_{hi} h_{t-1} + W_{ci} c_{t-1} + b_i) \eqlabel{LSTM_1}\\
 f_t &= \sigma(W_{xf} x_t + W_{hf} h_{t-1} + W_{cf} c_{t-1} + b_f) \eqlabel{LSTM_2}\\
 c_t &= f_t c_{t-1} + i_t \text{tanh}(W_{xc} x_t + W_{hc} h_{t-1} + b_c) \eqlabel{LSTM_3}\\
 o_t &= \sigma(W_{xo} x_t + W_{ho} h_{t-1} + W_{co} c_t + b_o) \eqlabel{LSTM_4}\\
 h_t &= o_t \text{tanh}(c_t) \eqlabel{LSTM_5}
\end{align}
where $\sigma$ is the logistic sigmoid function:
\begin{equation} \eqlabel{sigmoid_function}
s(x) = \frac{1}{1+e^x}
\end{equation}
and $i$, $f$, $o$ and $c$ are respectively the input gate, forget gate, output gate and cell state vectors, all of which are the same size as the hidden state vector $h$. These hidden state vector is a word-level embeddings, whereas $x_t$ is a normal word embedding. A sentence respectively a document is once read from left to right. We call this the Forward \LSTM with the correspond hidden states $\overrightarrow{h_t}$. We also read the sentence, respectively document from right to left. This is our Backward \LSTM with hidden states $\overleftarrow{h_t}$. The Bidirectional \LSTM is defined by the concatenation of both:
\begin{equation} \eqlabel{bidir_concatenation}
h_t = (\overrightarrow{h_t}, \overleftarrow{h_t})
\end{equation}
Thus a sentence embedding for the question is given by:
\begin{equation} \eqlabel{question_embedding}
q = (\overrightarrow{h_T}, \overleftarrow{h_1})
\end{equation}
with $T$ being the sentence length.
%
\subsubsection{Attention Mechanism}
In theory we can use the same technique we used for the sentence embedding for an entire document. In practice this does not work very good, as a document has to much information to encode into a single vector. The \LSTM also does not know about the question and is therefor unable to just concentrate on the relevant information. To overcome this we use an attention mechanism which is defined as follows:
\begin{align}
 a^h_t &= \sigma(W_{ha} h_t + W_{qa} q + b_a) \eqlabel{att_1}\\
 a^o_t &= \sigma(W_{aa} a^h_t + b_{a'}) \eqlabel{att_2}\\
 a_t   &= \frac{e^{a^o_t}}{\sum_{k=1}^T{e^{a^o_k}}} \eqlabel{att_3}
\end{align}
The attention mechanism consist of a hidden layer $a^h_t$ which gets the hidden state of the \LSTM $h_t$ and the encoding of the question $q$.
The output layer $a^o_t$ has a size of $1$.
A softmax function gives us the final attention weight $a_t$.
To get the document embedding we compute a weighted sum:
\begin{equation} \eqlabel{document_embedding}
d = \sum_{t=1}^T a_t h_t
\end{equation}
%
\subsubsection{Answer Generation}
To generate a single token answer we can simple plug $q$ and $d$ to a final \NN and predict the correct answer $s_1$ using a softmax layer over the entire output. To predict a sequence of token we have to use another \LSTM as described in \eqref{LSTM_1} to \eqref{LSTM_5}. Here we cannot us the word embedding $x_g$ as input to the \LSTM. Instead we use a document encoding $d$. To generate different words we have to change the attention mechanism so that it is aware of the previous decoder \LSTM state $h_g$.
\begin{align}
 a^h_{tg} &= \sigma(W_{ha} h_t + W_{h'a} h'_g + W_{qa} q + b_a) \eqlabel{att2_1}\\
 a^o_{tg} &= \sigma(W_{aa} a^h_{tg} + b_{a'}) \eqlabel{att2_2}\\
 a_{tg}   &= \frac{e^{a^o_{tg}}}{\sum_{k=1}^T{e^{a^o_{kg}}}} \eqlabel{att2_3}\\
 d_g      &= \sum_{t=1}^T a_{tg} h_t \eqlabel{att2_4}
\end{align}
Note that $h_t$ and $h'_g$ are two independent LSTMs, whereas $h_t$ is the encoder of the document and $h'_g$ is the decoder that generates the answer. To generate the answer sequence we now plug $h'_g$ and $x_{g-1}$ to a final \NN and predict the correct answer $s_g$.
%
\section{Extending the Attention Mechanism}

\section{Providing the Question}


function [J] = costFuncR1(W, E)

W_diff = (W * E) - W;
J = sum(sum(W_diff.^2));

end
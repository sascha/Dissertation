function [J] = costFuncR2(W, E, A, R)

Predict = (R * [(W * E); A]);
J = sum(sum(Predict.^2));

end
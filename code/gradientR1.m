function [J, grad_E] = gradientR1(W, E)

    % calculate error
    [J] = costFuncR1(W, E);
    
    grad_E = 2 * W' * ((W * E) - W);

end

function [J, grad_E, grad_A] = gradientR2(W, E, A, R)

    % calculate error
    [J] = costFuncR2(W, E, A, R);
    
    % precalc
    Predict = (R * [(W * E); A]);
    
    % calculate derivate for E
    %[row,column,value] = find(R);
    %delta = zeros(size(W));
    %for l=1:size(row)
    %    j = column(l);
    %    if (j > length(W))
    %        continue;
    %    end
    %    i = row(l);
    %    delta(j,:) = delta(j,:) + (value(l) * 2 * Predict(i,:));
    %end
    %delta = delta .* (W * E);
    %grad_E = delta' * W;
    
    rw = R(:,1:length(W)) * W;
    ra = R(:,length(W)+1:end) * A;
    grad_E = 2 * rw' * ((rw * E) + ra);

    % calculate derivate for A
    [row,column,~] = find(R);
    grad_A = zeros(size(A));
    for l=1:size(row)
        j = column(l) - length(W);
        if (j <= 0)
            continue;
        end
        i = row(l);
        grad_A(j,:) = grad_A(j,:) + (2 * Predict(i,:));
    end

end

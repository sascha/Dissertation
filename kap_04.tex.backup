\chapter{Word Embedding Calculus}
%
\section{Abstract}
We decompose a standard embedding space into interpretable
orthogonal subspaces and a ``remainder'' subspace. We
consider four
interpretable subspaces in this chapter:  polarity,
concreteness, frequency and
\POS subspaces. We introduce a new calculus
for subspaces that supports operations like ``$-1 \times
\mbox{\emph{hate}} = \mbox{\emph{love}}$'' and ``give me a
neutral word for \emph{greasy}'' (i.e.,
\emph{oleaginous}). This calculus extends analogy
computations like ``$\mbox{\emph{king}} - \mbox{\emph{man}}
+ \mbox{\emph{woman}} = \mbox{\emph{queen}}$''. For the
tasks of Antonym Classification and \POS Tagging our method
outperforms the state of the art. We create test sets for
Morphological Analogies and for the new task of Polarity
Spectrum Creation.

\section{Introduction}
Word embeddings are usually trained on an objective that
ensures that words occurring in similar contexts have
similar embeddings. This makes them useful for many tasks,
but has drawbacks for others; e.g., antonyms are often
interchangeable in context and thus have similar word
embeddings even though they denote opposites. If we think of
word embeddings as members of a (commutative or Abelian)
group, then antonyms should be \emph{inverses} of (as
opposed to \emph{similar} to) each other. In this chapter, we
use \Densifier~ proposed in the previous chapter to decompose a
standard embedding space into interpretable orthogonal
subspaces, including a one-dimensional polarity subspace as
well as concreteness, frequency and \POS subspaces. We
introduce a new calculus for subspaces in which antonyms are
inverses, e.g., ``$-1 \times \mbox{\emph{hate}} =
\mbox{\emph{love}}$''. The formula shows what happens in the
polarity subspace; the orthogonal complement (all the
remaining subspaces) is kept fixed. We show below that we
can predict an entire polarity spectrum based on the
subspace, e.g., the four-word spectrum \emph{hate}, \emph{dislike},
\emph{like}, \emph{love}. Similar to polarity, we
explore other interpretable subspaces and do operations such
as: given a concrete word like \emph{friend} find the
abstract word \emph{friendship} (concreteness); given the
frequent word \emph{friend} find a less frequent synonym
like \emph{comrade} (frequency); and given the noun
\emph{friend} find the verb \emph{befriend} (\POS).
%
\begin{figure*}[t!]
 \centering
%  \includegraphics[width=0.95\textwidth]{images/visualization_spectrum}
  \includegraphics[width=\textwidth]{images/visualization_spectrum}
  \caption{Illustration of the transformed embeddings. The
    horizontal axis is the polarity subspace. All
    non-polarity information, including concreteness,
    frequency and \POS, is projected into a two dimensional
    subspace for visualization (gray plane). A query word
    (bold) specifies a line parallel to the horizontal
    axis. We then construct a cylinder around this
    line. Words in this cylinder are considered to be part
    of the word spectrum.}  \figlabel{04_visualization}
\end{figure*}
%
\section{Setup and Method}
We use pretrained 300-dimensional English word embeddings \cite{mikolov2013efficient} (W2V). To train the transformation matrix, we use a combination of MPQA~\cite{wilson2005recognizing}, Opinion Lexicon~\cite{HuLiu04} and NRC Emotion lexicons~\cite{MohTur13} for polarity; BWK, a lexicon of 40,000 English words \cite{brysbaert2014concreteness}, for concreteness; the order in the word embedding file for frequency; and the training set of the \FLORS tagger \cite{schnabel2014flors} for \POS. The application of the transformation matrix to the word embeddings gives us four subspaces for polarity, concreteness, frequency and \POS. These subspaces and their orthogonal complements are the basis for an embedding calculus that supports certain operations. Here, we investigate four such operations. The first operation computes the antonym of word $v$:
\begin{equation} \eqlabel{antonym}
 \text{antonym}(v) = \text{nn}(u_v - 2u^p_v)
\end{equation}
where $\text{nn}: \mathbb{R}^{d} \rightarrow V$ returns the word whose embedding is the nearest neighbor to the input. Thus, our hypothesis is that antonyms are usually very similar in semantics except that they differ on a single ``semantic axis,'' the polarity axis.\footnote{See discussion/experiments below for exceptions} The second operation is ``neutral version of word $v$'':
\begin{equation} \eqlabel{neutral}
 \text{neutral}(v) = \text{nn}(u_v - u^p_v)
\end{equation}
Thus, our hypothesis is that neutral words are words with a value close to zero in the polarity subspace. The third operation produces the polarity spectrum of $v$:
\begin{equation} \eqlabel{spectrum}
 \text{spectrum}(v) = \{\text{nn}(u_v + x u^p_v) \mid \forall x \in \mathbb{R}\}
\end{equation}
This means that we keep the semantics of the query word fixed, while walking along the polarity axis, thus retrieving different shades of polarity. \figref{04_visualization} shows two example spectra. The fourth operation is ``word $v$ with \POS of word $w$'':
\begin{equation} \eqlabel{pos}
 \text{\POS}_w(v) = \text{nn}(u_v - u^m_v + u^m_w)
\end{equation}
This is similar to analogies like $\mbox{\emph{king}} - \mbox{\emph{man}} + \mbox{\emph{woman}}$, except that the analogy is \emph{inferred by the subspace relevant for the analogy}.

We create word spectra for some manually chosen words using the Google News corpus (W2V) and a Twitter corpus.
As the transformation was orthogonal and therefore did not change the length of a dimension, we multiply the polarity dimension with $30$ to give it a high weight, i.e., paying more attention to it.
We then use \eqref{spectrum} with a sufficiently small step size for $x$, i.e., further reducing the step size does not increase the spectrum. We also discard words that have a cosine distance of more than $.6$ in the non-polarity space. \tabref{examples} shows examples. The results are highly domain dependent, with Twitter's spectrum indicating more negative views of politicians than news. While \emph{fall} has negative associations, \emph{autumn}'s are positive -- probably because \emph{autumn} is of a higher register in American English.
%
\begin{table}[t!]
\centering
\begin{tabular}{l|l}
Corpus, Type & Spectrum\\\hline\hline
\multirow{3}{1.5cm}{News, Polarity} & hypocrite, \textbf{politician}, legislator, businessman, reformer, statesman, thinker\\\cline{2-2}
& fall, winter, summer, \textbf{spring}, autumn\\\cline{2-2}
& drunks, booze, liquor, lager, \textbf{beer}, beers, wine, beverages, wines, tastings\\\hline
\multirow{3}{1.5cm}{Twitter, Polarity} & corrupt, coward, \textbf{politician}, journalist, citizen, musician, representative\\\cline{2-2}
& stalker, neighbour, gf, bf, cousin, frnd, \textbf{friend}, mentor\\\cline{2-2}
& \#stupid, \#problems, \#homework, \#mylife, \#reality, \textbf{\#life}, \#happiness\\\hline
\multirow{2}{1.5cm}{News, Concreteness} & imperialist, conflict, \textbf{war}, Iraq, Vietnam War, battlefields, soldiers\\\cline{2-2}
& love, friendship, dear friend, friends, \textbf{friend}, girlfriend\\\hline
\multirow{2}{1.5cm}{News, Frequency} & redesigned, newer, revamped, \textbf{new}\\\cline{2-2}
& intellect, insights, familiarity, skills, \textbf{knowledge}, experience
\end{tabular}
\caption{Example word spectra for polarity, concreteness and
  frequency on two different corpora. Queries are bold.}
\tablabel{examples}
\end{table}
%
\section{Evaluation}
\subsection{Antonym Classification}
We evaluate on \newcite{adel2014}'s data; the task is to decide for a pair of words whether they are antonyms or synonyms. The set has 2,337 positive and negative pairs each and is split into 80\% training, 10\% dev and 10\% test. \newcite{adel2014} collected  positive/negative examples from the nearest neighbors of the word embeddings to make it hard to solve the task using word embeddings. We train an \SVM (\RBF kernel) on three features that are based on the intuition depicted in \figref{04_visualization}: the three cosine distances in: the polarity subspace;  the orthogonal complement; and  the entire space. \tabref{antonym_classification} shows that improvement of precision is minor (.76 vs.\ .75), but recall and $F_1$ improve by a lot (+.30 and +.16).
\begin{table}
\centering
\begin{tabular}{r|ccc|ccc}
 & \multicolumn{3}{c|}{dev set} & \multicolumn{3}{c}{test set} \\
 & $P$ & $R$ & $F_1$ & $P$ & $R$ & $F_1$ \\\hline\hline
Adel, 2014 & $.79$ & $.65$ & $.72$ & $.75$ & $.58$ & $.66$ \\
our work & $.81$ & $.90$ & $.85$ & $.76$ & $.88$ & $.82$ 
\end{tabular}
\caption{Results for Antonym Classification}
\tablabel{antonym_classification}
\end{table}
%
\def\florscolsepsmall{0cm}
\def\florscolseplarge{0.02cm}
\def\florscolsepverylarge{0.1cm}
%
\begin{table*}
\centering
\begin{tabular}{@{\hspace{\florscolsepsmall}}l@{\hspace{0.06cm}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l@{\hspace{\florscolseplarge}}|@{\hspace{\florscolsepverylarge}}l@{\hspace{\florscolsepsmall}}l}
&  & \multicolumn{2}{c}{newsgroups} & \multicolumn{2}{c}{reviews}&  \multicolumn{2}{c}{weblogs} & \multicolumn{2}{c}{answers}&  \multicolumn{2}{c}{emails} & \multicolumn{2}{c}{wsj}\\
&  & \multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}&\multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}&\multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}&\multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}&\multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}&\multicolumn{1}{c}{ALL}&\multicolumn{1}{c}{\OOV}\\\hline\hline
1 & LSJU 	& 89.11\sigd & 56.02\sigd  & 91.43\sigd & 58.66\sigd  & 94.15\sigd & 77.13\sigd  & 88.92\sigd & 49.30\sigd  & 88.68\sigd & 58.42\sigd  & 96.83 & 90.25 \\
2 & \SVM 	& 89.14\sigd & 53.82\sigd  & 91.30\sigd & 54.20\sigd  & 94.21\sigd & 76.44\sigd  & 88.96\sigd & 47.25\sigd  & 88.64\sigd & 56.37\sigd  & 96.63 & 87.96\sigd \\
3 & F  	& \textbf{90.86} & 66.42\sigd  & \textbf{92.95} & 75.29\sigd  & 94.71 & 83.64\sigd  & 90.30 & 62.15\sigd  & 89.44 & 62.61\sigd  & 96.59 & 90.37 \\\hline
4 & F+W2V	& 90.51 & \textbf{72.26} & 92.46\sigd & 78.03 & 94.70 & 86.05 & 90.34 & 65.16 & 89.26 & 63.70\sigd & 96.44 & 91.36 \\
5 & F+UD 	& 90.79 & 72.20 & 92.84 & \textbf{78.80} & \textbf{94.84} & \textbf{86.47} & \textbf{90.60} & \textbf{65.48} & \textbf{89.68} & \textbf{66.24} & \textbf{96.61} & \textbf{92.36} 
\end{tabular}
\caption{Results for \POS tagging. LSJU = Stanford.  \SVM =
  SVMTool. F=\FLORS. We show  three state-of-the-art taggers
(lines 1-3), \FLORS extended with
  300-dimensional embeddings (4) and extended with UD
  embeddings (5). $\dagger$: significantly better
  than the best result in the same column ($\alpha = .05$, one-tailed Z-test).}\tablabel{flors}
\end{table*}
%
\subsection{Polarity Spectrum Creation}
This task consists of two subtasks.
\PSC-SET: Given a query word how well can we predict a spectrum?
\PSC-ORD: How good is the order in the spectrum?
Our gold standard is Word Spectrum, included in the \OAWT and therefore also in MacOS. For each query word this dictionary returns a list of up to $80$ words of shades of meaning between two polar opposites. We look for words that are also present in \newcite{adel2014}'s Antonym Classification data  and retrieve $35$ spectra. Each word in a spectrum can be used as a query word; after intersecting the spectra with our vocabulary, we end up with 1301 test cases.

To evaluate \PSC-SET, we calculate the $10$ nearest neighbors of the $m$ words in the spectrum and rank the $10m$ neighbors by the distance to our spectrum, i.e., the cosine distance in the orthogonal complement of the polarity subspace. We report mean average precision (MAP) and weighted \MAP where each \MAP is weighted by the number of words in the spectrum. As shown in \tabref{spectrum} there is no big difference between both numbers, meaning that our algorithm does not work better or worse on smaller or larger spectra. 

To evaluate \PSC-ORD, we calculate Spearman's $\rho$ of the
ranks in \OAWT and the values on the polarity
dimension. Again, there is no significant difference between
average and weighted average of $\rho$. \tabref{spectrum}
also shows that the variance of the measures is low for
\PSC-SET and high for \PSC-ORD; thus, we do well on certain spectra and worse on others. The best one, \emph{beautiful} $\leftrightarrow$ \emph{ugly}, is given as an example. The worst performing spectrum is \emph{fat} $\leftrightarrow$ \emph{skinny} ($\rho=.13$) -- presumably because both extremes are negative, contradicting our modeling assumption that spectra go from positive to negative. We test this hypothesis by separating the spectrum into two subspectra.
We then report the weighted average correlation of the optimal separation.
For \emph{fat} $\leftrightarrow$ \emph{skinny}, this  improves $\rho$ to $.67$.
%
\begin{table}
\centering
\begin{tabular}{r|l@{\hspace{0.19cm}}l@{\hspace{0.19cm}}l}
 & \scriptsize{\PSC-SET: MAP} & \scriptsize{\PSC-ORD: $\rho$} & \scriptsize{$\text{avg}(\rho_1,\rho_2)$}  \\\hline\hline
average & $.48$ & $.59$ & $.70$\\
weighted avg. & $.47$ & $.59$ & $.70$ \\
variance & $.004$ & $.048$ & $.014$ \\\hline
beautiful$\leftrightarrow$ugly & $.48$ & $.84$ & $.84$ \\
fat$\leftrightarrow$skinny & $.56$  & $.13$ & $.67$ \\
absent$\leftrightarrow$present & $.43$  & $.72$ & $.76$
\end{tabular}
\caption{Results for Polarity Spectrum Creation:
MAP, Spearman's $\rho$ (one spectrum) and average $\rho$ (two subspectra)}\tablabel{spectrum}
\end{table}
%
\subsection{Morphological Analogy} 
The previous two subspaces were one-dimensional. Now we consider a \POS subspace, because \POS is not one-dimensional and cannot be modeled as a single scalar quantity. We create a word analogy benchmark by extracting derivational forms from \WordNet \cite{fellbaum1998}. We discard words with $\geq$2 derivational forms of the same \POS and words not in the most frequent 30,000. We then randomly select 26 pairs for every \POS combination for the dev set and 26 pairs for the test set.\footnote{This results in an even number of $25*26=650$ questions per \POS combination, $4*2*650=5200$ in total ($4$ \POS combinations, where each \POS can be used as query \POS).} An example of the type of equation we solve here is $\mbox{\emph{prediction}} - \mbox{\emph{predict}} + \mbox{\emph{symbolize}} = \mbox{\emph{symbol}}$ (from the dev set). W2V embeddings are our baseline.

We can also rewrite the left side of the equation as $\text{\POS}(\mbox{\emph{prediction}}) + \text{Semantics}(\mbox{\emph{symbolize}})$; note that this cannot be done using standard word embeddings. In contrast, our method can use meaningful UD embeddings and \eqref{pos} with $\text{\POS}(\mbox{\emph{v}})$ being $u_v^m$ and $\text{Semantics}(\mbox{\emph{v}})$ being $u_v - u_v^m$. The dev set indicates that a $8$-dimensional \POS subspace is optimal and \tabref{pos_analogy} shows that this method outperforms the baseline.
%
\begin{table}
\centering
\begin{tabular}{r|l@{\hspace{0.1cm}}r|l@{\hspace{0.1cm}}r}
 & \multicolumn{2}{c|}{W2V} & \multicolumn{2}{c}{UD} \\
 & A$\rightarrow$B & B$\rightarrow$A & A$\rightarrow$B & B$\rightarrow$A \\\hline\hline
% DEV SET
%noun-verb  & 32.93 &  8.31 & 47.38 & 46.92 \\
%adj-noun   & 48.46 & 54.77 & 72.00 & 66.62 \\
%adj-verb   & 18.00 &  3.23 & 27.38 & 13.85 \\
%adj-adverb & 40.00 & 43.54 & 42.46 & 43.54 \\\hline
%all & \multicolumn{2}{c|}{25.15} & \multicolumn{2}{c}{45.02}
% TEST SET
noun-verb  & 35.69 &  6.62 & 59.69\sigd & 50.46\sigd \\
adj-noun   & 30.77 & 27.38 & 53.85\sigd & 43.85\sigd \\
adj-verb   & 20.62 &  3.08 & 32.15\sigd & 24.77\sigd \\
adj-adverb & 45.38 & 35.54 & 46.46\signo & 43.08\sigd \\\hline
all & \multicolumn{2}{c|}{25.63} & \multicolumn{2}{c}{44.29\sigd} 
\end{tabular}
\caption{Accuracy @1 on test  for Morphological Analogy. $\dagger$: significantly better
  than the corresponding result in the same row ($\alpha = .05$, one-tailed Z-test).}
\tablabel{pos_analogy}
\end{table}
%
\subsection{Part-Of-Speech Tagging}
Our final evaluation is extrinsic. We use \FLORS
\cite{schnabel2014flors}, a state-of-the-art \POS tagger
which was extended by \newcite{DBLP:conf/emnlp/YinSS15} with
word embeddings as additional features. W2V  gives us a consistent improvement on \OOV words (\tabref{flors}, line 4). However, training this model requires about 500GB of RAM. When we use the 8-dimensional UD embeddings (the same as for Morphological Analogy), we  outperform W2V except for a virtual tie on news (\tabref{flors}, line 5). So we perform better even though we only use 8 of 300 dimensions! However, the greatest advantage of UD is that we only need 100GB of RAM, 80\% less than W2V.

\section{Related Work}
\newcite{yih2012polarity} also tackled the problem of antonyms having similar  embeddings. In their model, the antonym is the inverse of the entire vector whereas in our work the antonym is only the inverse in an ultradense subspace. Our model is more intuitive since antonyms invert only part of the meaning, not the entire meaning. \newcite{schwartz-reichart-rappoport:2015:CoNLL} present a method that switches an antonym parameter on or off (depending on whether a high antonym-synonym similarity is useful for an application) and learn \emph{multiple} embedding spaces. We only need a \emph{single} space, but consider different subspaces of this space.

An unsupervised approach using linguistic patterns that
ranks adjectives according to their intensity was presented
by \newcite{TACL48}. \newcite{sharma-EtAl:2015:EMNLP}
present a corpus-independent approach for the same
problem. Our results (\tabref{examples}) suggest that
polarity should not be considered to be corpus-independent.

There is also much work on incorporating the additional
information into the original word embedding
training. Examples include \cite{botha2014compositional} 
and \cite{cotterell-schutze:2015:NAACL-HLT}.
However,
postprocessing has several
advantages. \Densifier~can be trained on a normal work
station without access to the original training corpus. This
makes the method more flexible, e.g., when new training data
or desired properties are available.

On a general level, our method bears some resemblance with
\cite{weinberger09metriclearning} in that we perform
supervised learning on a set of desired (dis)similarities 
and that we can think of our method as learning specialized
metrics for particular subtypes of linguistic information or
particular tasks. 
Using the method of 
\newcite{weinberger09metriclearning},
one could learn $k$ metrics for $k$ subtypes of
information and then simply represent a word $w$ as the
concatenation of (i) the original embedding and
(ii) $k$ representations corresponding to the $k$
metrics.\footnote{We would like to thank an anonymous
  reviewer for suggesting this alternative approach.} In
case of a simple one-dimensional type of information, the
corresponding representation could simply be a scalar.
We would expect this approach to have similar advantages for
practical applications, but we view our orthogonal
transformation of the original space as more elegant and it
gives rise to a more compact representation.

\section{Summary}
We presented a new word embedding calculus based on
meaningful ultradense subspaces. We applied the operations
of the calculus to Antonym Classification, Polarity Spectrum
Creation, Morphological Analogy and \POS Tagging. Our
evaluation shows that our method outperforms previous work
and is applicable to different types of information. 
We have published test sets and word embeddings at \url{http://www.cis.lmu.de/~sascha/Ultradense/}.


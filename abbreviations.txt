A CBOW Continuous Bag-Of-Words
A CNN Convolutional Neural Network
A LSTM Long Short-Term Memory Network
A MAP Mean Average Precision
A MEE Multi-Edge Extraction
A MFS Most Frequent Synset
A MRR Mean Reciprocal Rank
A NER Named Entity Recognition
A NLP Natural Language Processing
A NN Neural Network
A OAWT Oxford American Writer's Thesaurus
A OOV Out-Of-Vocabulary
A PCA Principal Component Analysis
A POS Part-Of-Speech
A PPR Personalized PageRank
A PSC Polarity Spectrum Creation
A RBF Radial Basis Function
A RNN Recurrent Neural Network
A SALSA
A SGD Stochastic Gradient Descent
A SVM Support Vector Machine
A WINTIAN WINner-Takes-It-All Network
A WSD Word Sense Disambiguation

G ADMM
G AutoExtend is a method to extend word embeddings to embeddings for other linguistical units
G BWK
G CoSimRank is a similarity measure for nodes in a graph presented in this work
G CSFD is a Czech movie review dataset
G Densifier is a method to learn an orthogonal transformation of an embedding space
G FEEL
G FLORS is a part-of-speech tagger
G FrWac
G GUR50 is a word similarity dataset for German
G GUR650 is a word similarity dataset for German
G HITS Hyperlink-Induced Topic Search also known as hubs and authorities is a link analysis algorithm 
G IMS It Makes Sense is a supervised word sense disambiguation system
G lingCNN is a linguistical informed CNN for polarity classification
G LSJU
G MC is a word similarity dataset
G MEN is a word similarity dataset
G MPQA
G NRC
G RG is a word similarity dataset
G SCWS is a word in context similarity dataset
G SIMLEX is a word similarity dataset
G SimRank is a similarity measure for nodes in a graph
G SVD Singular Value Decomposition
G WHM
G WORDSIM is a word similarity dataset
G ZG222 is a word similarity dataset for German
G PageRank is an algorithm to rank nodes in a graph
G GermaNet is a lexical-semantic net that relates German nouns, verbs, and adjectives
G Freebase is a large collaborative knowledge base
G WordNet is a lexical-semantic net that relates English nouns, verbs, and adjectives
G Senseval is an word sense evaluation series
G SemEval is an series of evaluations of computational semantic analysis systems

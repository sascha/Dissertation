\chapter{Introduction}

Word representations, also called word embeddings, are representations of words as mathematical objects.
They allow us to use words in Natural Language Processing,
e.g., as input to neural networks.
An additional benefit is that we can handle rare or even unseen events, e.g., by considering nearest neighbors.
The formal definition of an embedding is often given as an
injective and structure-preserving map $f : V \rightarrow U$
where
$V$ is the discrete space of words -- the vocabulary.

A simple word embedding is a one-hot vector.
In this case, we create a vocabulary of the $n$ most frequent words.
Typical values for $n$ are between 10,000 and 100,000.
The word embedding of word $v$ is then given by the canonical unit vector $e_v \in \mathbb{R}^{|V|}$.
We can now use these embeddings in neural networks or other applications.

However, one-hot vectors do not have the advantage of generalization.
For example, given the phrase \textit{the fast car}, we can learn that a \textit{car} is an object that moves, but this phrase would tell us nothing about the word \textit{cars}.
As all embeddings have the same distance to each other it is impossible to incorporate information of the nearest neighbors.
In this thesis, we will use two methods to overcome this issue.
\begin{enumerate}
	\item The first approach maps high dimensional
          sparse one-hot vectors to low dimensional dense
          vectors. This forces the system to generalize. For
          example, the system will learn that \textit{car} and \textit{cars} are very similar as they occur in similar contexts. Because the generalization is automatically learned from a training corpus, we call this method unsupervised.
	\item The second approach generates a graph, where words are nodes and edges are constructed by linguistic relationships like direct object (e.g., \textit{drive-car}) or hypernymy (e.g., \textit{vehicle-car}). These relationships have to be defined and we therefore call this method supervised. After defining the types of relations we can manually create the graph using linguists or crowdsourcing. We can also learn the edges and nodes by scanning through a large annotated corpus.
\end{enumerate}

In Natural Language Processing, the mathematical definition
we just gave is not always applied strictly.
For example, there is often no formal constraint that two different words cannot be mapped to the same vector.
This would violate the injectivity.
However, injectivity is still a useful property, as it guarantees that we are still able to distinguish words.
This also includes synonyms, where we want to have the resulting objects to be similar but not identical.
The property of structure preservation is not clearly defined.
For example, sometimes we want antonyms to be similar and sometimes we want antonyms to be inverses of each other.

In this introduction, we will briefly cover unsupervised embedding learning methods, some basics of graph theory to handle supervised embeddings and the evaluation of word embeddings.
Finally, we will introduce task specific embeddings which can be seen as building blocks of deep neural networks, a subject currently undergoing intense study.

\section{Unsupervised Training Methods}

Among the earliest work on distributed word representations, usually called word embeddings today, was \cite{rumelhart1988learning}.
Non-neural-network techniques that create low-dimensional word representations also have been used widely, including singular value decomposition (SVD) \cite{deerwester90indexing,schutze92dimensions} and random indexing \cite{kanerva88sparse,kanerva09hyper}.
There has been a resurgence of work on embeddings recently (e.g., \newcite{bengio2003neural}, \newcite{mnih2007three}, \newcite{collobert11scratch}), including methods that are SVD-based \cite{levy14neural,stratos15decompositions}.
We will cover the three most frequently used word embedding methods in this section, namely Continuous Bag of Word (CBOW), Skip-gram and Global Vectors (GloVe).

\subsection{Continuous Bag of Words}

The Continuous Bag of Word method, often just called CBOW, was introduced together with Skip-gram (see next subsection) by \newcite{mikolov2013efficient}.
Both methods are part of a highly optimized toolkit called word2vec \cite{mikolov2013distributed} and are similar to an autoencoder.
First, we have to define a vocabulary $V$ together with an index function $v$ that maps from a position in the training corpus to the index of the corresponding word in the vocabulary.
We are able to set the size of the vocabulary quite high. It typically includes 100,000 to 1,000,000 words.
Out of vocabulary words (OOVs) can either be replaced by a special token and mapped to the corresponding index or simply be removed from the training corpus.
The method also defines a window size $k$ around a word at position $t$.
CBOW then uses all words at position $t+k'$, with $0 < |k'| \leq k$, i.e., all words inside the window excluding the middle word, to predict the word in the middle.

Let $e_{v(t)} \in \mathbb{R}^{|V|}$ be a canonical unit vector of length $|V|$ where $|V|$ is the number of words in our vocabulary.
This vector is also called the one-hot representation of the word at position $t$.
CBOW learns two matrices $W, C \in \mathbb{R}^{d \times|V|}$ where $d$ is the dimensionality of the embedding space we want to learn.
This parameter has to be defined prior to the training.
Typical values for the dimensionality of the embeddings space are $50$, $100$, $300$ or $500$.
We can now state the learning objective of CBOW as follows:

\begin{equation}\eqlabel{cbow}
\underset{W,C}{\operatorname{argmin}} \left\| \left( \sum_{0 < |k'| \leq k} \frac{1}{2k}(W e_{v(t+k')}) \right) - C e_{v(t)}\right\| \qquad ,\forall t
\end{equation}

This is illustrated in \figref{cbow}.
As all neural networks, the training is performed using the gradient descent algorithm.
In contrast to most neural network implementations which use batch gradient descent, a stochastic gradient descent is used.
However, the training can be parallelized to multiple GPUs.
A corpus of several billion words can therefore be processed in under one day.
A word embedding of word $v$ is given by $W e_{v}$, i.e., the columns of $W$ define the word embeddings. The values in $C$ are discarded.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/cbow}
\caption{The CBOW algorithm takes the entire context to predict the word in the middle word.}
\figlabel{cbow}
\end{figure}

\subsection{Skip-Gram}

CBOW uses the context words to predict the word in the middle.
Skip-gram vice versa uses the word in the middle to predict surrounding words at position $t+k'$, with $0 < |k'| \leq k$.
The learning objective of Skip-gram (\figref{skipgram}) is given as follows:

\begin{equation}\eqlabel{sg}
\underset{W,C}{\operatorname{argmin}} \sum_{0 < |k'| \leq k} \|W e_{v(t)} - C e_{v(t+k')}\| \qquad ,\forall t
\end{equation}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{images/skipgram}
\caption{The Skip-gram algorithm iterates through all context words and pairs one at a time with the middle word.}
\figlabel{skipgram}
\end{figure}

Theoretically \eqref{cbow} and \eqref{sg} are equivalent when $W$ and $C$ are switched.
However, there are practical issues which further distinguish both methods:

\begin{enumerate}
 \item The window size $k$ is randomly sampled between $0$ and $k$ for each gradient descent step.
 \item Whereas $W$ is initialized with a uniform distribution, $C$ is set to a zero matrix. This results in slightly shorter word embeddings for vectors in $W$. However, this effect is negligible for frequent words or if the training lasts for a lot of iterations. 
 \item CBOW performs one back propagation per middle word whereas Skip-gram performs one for every word in the window. Because of this CBOW is also notably faster than Skip-gram especially when the window size $k$ is large.
 \item To optimize the learning process, methods like hierarchical softmax or negative sampling are used. For details on these methods see \cite{mikolov2013efficient} and \cite{mikolov2013distributed}
\end{enumerate}

\subsection{Global Vectors}

Global Vectors, often just called GloVe, are embeddings that were introduced by \newcite{pennington2014glove} and aim to combine the advantages of matrix factorization and local context windows. For this, a co-occurrence matrix $X$ is defined, with $X_{ij}$ being the number of times word $j$ occurs in the context of word $i$. It follows that the probability of word $j$ occurring in the context of word $i$ is given by:

\begin{equation}\eqlabel{prob}
P_{ij} = \frac{X_{ij}}{\sum_k X_{ik}} 
\end{equation}

The idea of the model is that word embeddings should be based on the ratio of co-occurrence probabilities and not on the co-occurrence itself.
That means if we want to learn something about the relationship of word $i$ and $j$ we don't look at $P_{ij}$ but on $P_{ik}/P_{jk}$ for all $k$.
Further constraints on the model are linearity and homomorphy. This results in the final learning objective:

\begin{equation}\eqlabel{glove}
\underset{W,C}{\operatorname{argmin}}  \|(W e_i)^T C e_j + b_i + b_j - \log(X_{ij})\| \qquad ,\forall i,j
\end{equation}

with $b_i$ and $b_j$ being bias terms.
In this model, the context vectors are kept for a small boost in performance.
The final word embedding is a combination of both vectors, i.e.:
$$(\lambda W + (1-\lambda) C) e_{v}$$

\section{Graph-Based Word Embeddings}

The first publication in Chapter 2 covers graph-based embeddings.
In this section, we will introduce some basics of graph theory and how graphs fit in our framework of word embeddings.
So be $U = \{1, 2, 3, \ldots, n\}$ the set of nodes.
The set of edges $E$ is a subset of $U \times U$. The pair $G=(U, E)$ is called graph. The edges can be defined by grammatical relationships, e.g., verb-object pairs or hypernymy.
Graphs based on these definitions were created by \newcite{dorow2009} and called WordGraph.
They used the following relationships for edges: adjectival modifier (adjective-noun), direct object (verb-object) and noun coordination (noun-noun).
They were parsed from Wikipedia for two languages, English and German.
When a relationship is above a certain threshold an edge was created.
The graphs also included node types, defined by POS; edge types, defined by the different grammatical relationships; and edge weights, defined by the logarithm of the log-likelihood score.

Another graph of words is WordNet by \newcite{fellbaum1998}.
This graph was manually created and the edges are defined by conceptual-semantic and lexical relations.
These include hypernymy (superset), hyponymy (subset), meronymy (part-of), holonymy (opposite of meronymy).
Additionally, words are grouped into synsets, so we can easily construct a synonym relationships between words.
We will use WordNet in this thesis together with GermaNet by \newcite{hamp1997germanet}, which is a similar resource for German, and Freebase \cite{bollacker2008freebase}, which is a graph-like resource with an emphasis on entities.

To fit in the framework of word embeddings we can simply define the embedding map $f : V \rightarrow U$.
However, a mapping to nodes is only slightly useful as most evaluations and applications, e.g., neural networks, rely on real-valued vectors.
To overcome this limitation we will propose a similarity measure based on the Personalized PageRank.
The Personalized PageRank assigns a vector to each node.
So we can also use this method in other evaluations or applications.
The new method is presented in Chapter 2 but we will introduce the basics -- PageRank and Personalized PageRank -- in following two subsections.
We will put an emphasis on the mathematical foundations, i.e., existence and uniqueness of solutions.

\subsection{PageRank}

PageRank was developed 1997 by Lawrence Page and Sergei Brin and registered for patent. The name stands for his inventor as well as its primary usage, i.e., ranking web pages. The computation of the PageRank can be done analytically by solving the eigenvalue problem of a matrix similar to an adjacency matrix. To overcome time and space complexity an iterative computation is used in practice. The PageRank of node $i$ at iteration $k+1$ is defined as:

\begin{equation}\eqlabel{pagerank_iteration}
	P_i^{(k+1)} = \sum \limits_{(j,i)\in E}\frac{P_j^{(k)}}{g_{out}(j)}
\end{equation}

where $g_{out}(u) = |\{x \in U|(u,x) \in E\}|$ is the outdegree of node $u$. The computation is started with $P_i^0 = \frac{1}{n}$ for all $i \in E$.
The intuitive idea is that pages (i.e., nodes) pass weight through links (i.e., edges) to other web pages. This means that a web page is important if it has important web pages linking to it.
Another interpretation is the random surfer model, where an internet surfer randomly clicks links on web pages. The PageRank then corresponds to the probability of that surfer being on a certain page.
We can rewrite \eqref{pagerank_iteration} into a matrix formalization:

\begin{align}\eqlabel{matrix_iteration}
p^{(k+1)} &= Ap^{(k)} \\
p^{(0)} &= \frac{1}{n} j_n = \left(\frac{1}{n}, \ldots, \frac{1}{n}\right)^T
\end{align}

The matrix $A$ is the column normalized adjacency matrix and $j_n$ is a vector of ones and length $n$.
If a node has no outgoing edges we set the corresponding column in $A$ to $\frac{1}{n} j_n$.
To ensure fast convergence $A$ must also be strictly positive, which is why an additional damping factor $d$ is introduced.

\begin{equation}\eqlabel{google_matrix}
	G = dA + (1 - d)\left(\frac{1}{n}J_n\right)
\end{equation}

with $J_n$ being a square matrix of ones and size $n$. The matrix $G$ is also called Google matrix. The iterative computation is then given by:

\begin{align}\eqlabel{matrix_iteration2}
p^{(k+1)} &= Gp^{(k)} \\
p^{(0)} &= \frac{1}{n} j_n = \left(\frac{1}{n}, \ldots, \frac{1}{n}\right)^T
\end{align}

with G being column normalized and strictly positive the following statements hold true \cite{perron1907theorie}:
\begin{enumerate}
	\item The spectral radius $\rho(A) > 0$
	\item $\rho(A)$ is a single eigenvalue
	\item There exists a vector $p$ so that $Ap = \rho(A)p$, $p > 0$ and $\|p\|_1 = 1$
	\item $A$ does not have other eigenvectors $v > 0$ (except for multiples of $p$)
\end{enumerate}

It is also known that the iteration in \eqref{matrix_iteration} converges to the eigenvector corresponding to the dominant eigenvalue, i.e., the spectral radius \cite{mises1929praktische}. We therefore have a unique and well-defined solution $p$ for the iterative computation of PageRank. The vector $p$ is also called Perron vector.

\subsection{Personalized PageRank}

The PageRank assigns each node in the graph a positive value.
We want to have a more discrete variable and define the Personalized PageRank $p_i$.
We change \eqref{matrix_iteration} as follows:

\begin{align}
p_i^{(k+1)} &= dAp_i^{(k)} + (1 - d)e_i\\
p_i^{(0)} &= e_i
\end{align}

with $e_i$ being the canonical unit vector. Note that this would result in a non-negative but not strictly positive matrix for the vector iteration.
However, we can postulate the same conclusions as in the previous section if $A$ is irreducible \cite{frobenius1912matrizen}.
The adjacency matrix of a graph $G$ is irreducible if and only if $G$ is connected, i.e., there is a path between every pair of nodes.
WordGraph is undirected and in WordNet, every edge type also has an inverse type, e.g., hypernymy and hyponymy.
This condition can be considered to be fulfilled for the graphs we are using.

\subsection{SimRank}

In the previous subsection, we described how to map nodes in a graph to a real-valued vector.
These vectors can already be used to compute the similarity between nodes, by using an arbitrary metric of the vector space.
We will cover this in more detail in the next section.
However, we can also apply a similarity measure directly to the graph.
The most used methods for this is SimRank developed by \newcite{jehwidom2002} and the idea is similar to PageRank.
The intuition that important nodes are connected to important nodes translates to \textit{nodes are similar if their neighbors are similar}.
We can write the iterative matrix formalization as follows:

\begin{align}\eqlabel{simrank_matrix_iteration}
S^{(k+1)} &= dAS^{(k)}A^T \\
S^{(0)} &= E
\end{align}

with $E$ being the identity matrix and $d$ being a decay factor similar to the decay factor used in \eqref{google_matrix}.
Obviously, this computation converges to zero if $d < 1$.
To prevent this the diagonal of $S^k$ is set to one after each iteration.
It is now easy to see that SimRank converges to a nontrivial matrix. For a proof please see \cite{jehwidom2002} or chapter 2 of this thesis where we prove the convergence of CoSimRank from which the convergence of SimRank immediately follows.
The downside of resetting the diagonal after each iteration is that we do not have a local formulation of SimRank, i.e., we always have to compute all similarities in the graph.
We will overcome this by introducing CoSimRank.

\section{Intrinsic Evaluation}

As we already mentioned word embeddings shall preserve the structure of the word space. There is no clear definition of the structure of a word space but we might want to preserve similarity, i.e., similar words are mapped to similar word embeddings. This is the basis for the first popular intrinsic task to evaluate word embeddings \cite{miller1991contextual}. The second evaluation, namely Word Analogy, tests the preservation of relations \cite{mikolov2013efficient}. We will cover these two evaluation methods in the following subsections. Other evaluations include Word Categorization, i.e., clustering the words into different categories, and selectional preference. Here the task is to determine if a verb-noun pair is a verb-object or a verb-subject pair. For example, is it \textit{drive a car} or \textit{a car drives}. Another task is to find an outlier among a group of words, e.g., \textit{apple}, \textit{banana}, \textit{moon} and \textit{peach}.

\subsection {Word Similarity}
In this task, a set of word pairs is given together with human-assigned similarity scores.
Word Similarity test sets for the English language include MC \cite{miller1991contextual}, MEN \cite{bruni2014multimodal}, RG \cite{rubenstein1965contextual}, SIMLEX \cite{hill2014simlex}, RW \cite{luong2013better} and WordSim-353 \cite{finkelstein2001placing}.
\tabref{ws_examples} gives some examples.
The human assigned score is usually based on several annotators.
To evaluate the word embeddings we have to define a similarity measure in the embedding space.
A frequent choice is the cosine similarity defined by:

\begin{equation}\eqlabel{cosine}
\operatorname{sim}_{\operatorname{cos}}(v, w) = \frac{v^T w}{\|v\| \|w\|}
\end{equation}

or euclidean similarity defined by:

\begin{equation}\eqlabel{euclidean}
\operatorname{sim}_{\operatorname{L}^2}(v, w) = \frac{1}{1 + \|v - w\|}
\end{equation}

\begin{table}
\centering
\begin{tabular}{r|l|l}
\textbf{word 1} & \textbf{word 2} & \textbf{similarity}\\\hline
coast & shore & 9.10\\
money & dollar & 8.42\\
tiger & cat & 7.35\\
psychology & clinic & 6.58\\
energy & crisis & 5.94\\
word & similarity & 4.75\\
situation & isolation & 3.88\\
opera & industry & 2.63\\
stock & egg & 1.81\\
professor & cucumber & 0.31
\end{tabular}
\caption{Examples of word similarity test pairs, taken out of WordSim-353.}
\tablabel{ws_examples}
\end{table}

After this, we compute a correlation between the human scores and the word embedding based scores.
The most basic correlation is Pearson's \textit{r} \cite{galton1888co}.

\begin{equation}\eqlabel{pearson}
\rho_p(X,Y) = \frac{\operatorname{cov}(X,Y)}{\sigma_{X}\sigma_{Y}}
\end{equation}

The downside of the Pearson's \textit{r} is, that it measures the linear dependency between two variables.
This is an unnecessary constraint to our models and we therefore use rank correlations.
A lot of publications report the Spearman's \textit{rho} \cite{spearman1904proof} which is defined by computing Pearson's \textit{r} of the ranked variables:

\begin{equation}\eqlabel{spearman}
\rho_s(X,Y) = \rho_p(\operatorname{rg}_X,\operatorname{rg}_Y) = \frac{\operatorname{cov}(\operatorname{rg}_X,\operatorname{rg}_Y)}{\sigma_{\operatorname{rg}_X}\sigma_{\operatorname{rg}_Y}}
\end{equation}

where \textit{rg} converts scores to ranks, \textit{cov} is the covariance and $\sigma$ is the standard deviation.
Sometimes also Kendall's tau is reported \cite{kendall1948rank}.
For this, we consider all pairs of observations $(x_i,y_i)$ and $(x_j,y_j)$.
With no loss of generality, we set $x_i < x_j$.
We call a pair concordant if $y_i < y_j$ and discordant if $y_i > y_j$.
If $x_i = x_j$ or $y_i = y_j$ the pair is called neither concordant nor discordant.
With $n$ being the number of observations, Kendall's tau is defined as follows:

\begin{equation}\eqlabel{kendall}
\tau(X,Y) = \frac{(\text{\# of concordant pairs})-(\text{\# of discordant pairs})}{n(n-1)/2}
\end{equation}

A high correlation indicates high-quality word embeddings.
While we already discard absolute values by using a rank correlation this still yields problems.
For example, it is obvious that the word pair \textit{car-bus} shall get a higher similarity than \textit{car-table}.
However, depending on the context we want to investigate, it is unclear if \textit{bus-train} (both a transportation vehicle) or \textit{bus-truck} (both a big vehicle driving on a road) shall get a higher similarity.
Further problems were highlighted by \newcite{faruqui2016problems}.

\subsection{Word Analogy}
For this task, two pairs of words were grouped together.
Each pair is related through the same relationship, e.g., country-capital or adjective-comparative. 
One out of the four words is hidden and the task is to predict the hidden word.
This can be solved by computing an offset vector between one pair and adding it to the single word.
A well-known example is the set king-men and \textit{queen-women}.
The word \textit{queen} can be found by solving:

\begin{equation}\eqlabel{kingqueen}
f(\text{king}) - f(\text{men}) + f(\text{women}) = f(\text{queen})
\end{equation}

whereas $f$ is the word embedding function that maps a word to a word embedding.
Note that we either compute the offset vector between \textit{men} and \textit{women} or between \textit{men} and \textit{king}.
Both variants lead to the same equation.
More examples can be seen in \tabref{wa_examples}.

\begin{table}
\centering
\begin{tabular}{l|l|l|l}
\textbf{word 1A} & \textbf{word 1B} & \textbf{word 2A} & \textbf{word 2B}\\\hline
Paris & France & Berlin & Germany \\
Europe & euro & USA & dollar \\
brother & sister & policeman & policewoman \\\hline
amazing & amazingly & obvious & obviously\\
slow & slower & cold & colder\\
fast & fastest & tasty & tastiest\\
discover & discovering & swim & swimming\\
China & Chinese & Mexico & Mexican\\
predicting & predicted & going & went\\
man & men & eye & eyes\\
work & works & go & goes
\end{tabular}
\caption{Examples of word analogy test cases. The upper part is of semantic nature whereas the lower part is of syntactic nature.}
\tablabel{wa_examples}
\end{table}

After computing the left side of \eqref{kingqueen} we have to find the nearest neighbors to the result.
Cosine distance or euclidean distance is used.
If using the euclidean distance this would result in the following optimization problem:

$$\underset{v \in V}{\operatorname{argmin}} \quad \|f(a) - f(b) + f(c) - f(v)\|$$

To simplify the notation we will use $v$ for a word as well as for the corresponding word embedding $f(v)$.
Some implementations normalize all word embeddings first and compute the cosine similarity.
This is equivalent to the following optimization problem:

$$\underset{v \in V}{\operatorname{argmax}} \quad \operatorname{sim}_{\operatorname{cos}}(v, a) - \operatorname{sim}_{\operatorname{cos}}(v, b) + \operatorname{sim}_{\operatorname{cos}}(v, c)$$

In our example, this means that we seek a word which is similar to \textit{king} and \textit{woman} but dissimilar to \textit{men}.
An alternative to the computation of the offset vector was proposed by \newcite{levy2014linguistic}.

$$\underset{v \in V}{\operatorname{argmax}} \quad \frac{\operatorname{sim}_{\operatorname{cos}}(v, a) \operatorname{sim}_{\operatorname{cos}}(v, c)}{\operatorname{sim}_{\operatorname{cos}}(v, b)}$$

The method is designed to increase the differences between small quantities and reducing the differences between larger ones.
Extra care has to be taken to avoid divisions by zero, e.g., by mapping all cosine similarities to $[0, 1]$ and adding $\epsilon$ to the division.

\tabref{nn_examples} gives examples of the nearest neighbors using the cosine distance.
The final evaluation score is simply the accuracy where the correct word was found in position one.
A higher accuracy indicates higher quality word embeddings.
Current state-of-the-art models achieve an accuracy above 70\%.
Sometimes also acc@10 is reported, i.e., how often the correct word was found in the list of top 10 candidates.
If the list is of arbitrary size or there is more than one correct answer, precision, recall and F1 are also common measures.
As we have to find the nearest neighbor the computation of Word Analogy is slightly more expensive than Word Similarity, where we just have to compute a cosine similarity.
Word Analogy tests are therefore often reduced to most frequent words, e.g., top 30,000.

\begin{table}
\centering
\begin{tabularx}{\linewidth}{r|X}
\textbf{query} & \textbf{nearest neighbors}\\\hline
Paris & Heidi, London, France, Dubai, Samuel, Hilton, Rome, Toronto, Las Vegas, Lindsay Lohan \\\hline
Europe & European, Germany, Spain, England, America, USA, France, Greece, Italy, Sweden, Africa \\\hline
policeman & policewomen, policemen, constable, cop, taxi driver, soldier, sergeant, police, patrolman \\\hline
amazing & incredible, awesome, unbelievable, fantastic, phenomenal, astounding, wonderful, remarkable, marvelous, fabulous \\\hline
fast & quick, rapidly, quickly, slow, faster, rapid, speed \\\hline
lightning + thunder & thunderstorm, rain, heavy rain, roar, blazing \\\hline
Friday + Sunday & Saturday, Thursday, Monday, Wednesday, Tuesday, afternoon \\\hline
Berlin - Germany + France & Paris, French, Brussels, Rome, Toulouse \\\hline
king - man + woman & queen, monarch, princess, prince, kings, monarchy \\\hline
slower - slow + cold & colder, warmer, cooler, chilly, frigid, chill \\\hline
\end{tabularx}
\caption{Examples for the nearest neighbors of word embedding expressions. Note that nearest neighbors include synonyms and antonyms. Ambiguous words include related words of both meanings.}
\tablabel{nn_examples}
\end{table}

Both evaluations, Word Similarity and Word Analogy, are appealing because they are easy to implement and inexpensive to compute.
They are also usually a good first indicator.
However, they may not always be consistent with performance on downstream tasks \cite{schnabel2015evaluation}.
Because of this extrinsic evaluations measure the performance on a certain task, e.g., POS Tagging or Sentiment Analysis \cite{nayak2016evaluating}.
On the other side, if word embeddings are optimized for a certain task the embedding training can also be integrated into the learning algorithm.
This produces task specific embeddings, which we will cover in the next section.

\section{Task Specific Embeddings}
We already showed how to learn word embeddings on unlabeled data.
These embeddings were thought to be general and not specific to any task.
The idea of these embeddings is that unlabeled corpora are widely available and the information contained in them can be transferred to tasks where less training data is available.
However, there are also tasks where we do have a lot of training data, e.g., machine translation for popular languages or question answering.
For these applications, we can also learn task specific embeddings on the fly while training an end-to-end system. In this section, we will briefly discuss word-, word-level-, sentence- and document-embeddings for machine comprehension, i.e., where we have a document a question and an answer.

\figref{attentive_reader} gives an example of how such advanced embeddings can be used in a question-answering system.
We end this introduction with these advanced embeddings and this architecture to give an outlook on where the area of natural language processing is headed
that this thesis makes a contribution on.

\subsection{Word Embeddings}
Word Embeddings can be learned in a downstream task by simply connecting the one-hot vector corresponding to the word via a matrix multiplication to the input of the neural network.
Or equivalently we use a lookup table which stores a vector for each word in the vocabulary.
To use pre-trained word embedding, like CBOW, we can initialize this lookup table with the pre-trained word embeddings.
Often both approaches are combined by initializing the lookup table with pre-trained word embeddings but updating them via the training process.

\subsection{Word-level Embeddings}
In contrast to word embeddings, word-level embeddings are not unique for all occurrences of one word.
Instead, they also depend on the context.
In terms of precise notation, we have to be careful as this embedding is not a function with the vocabulary $V$ as the domain but only a relation between vocabulary and embedding space or alternative a function $f: V^l \rightarrow \mathbb{R}^{d}$.
The most common approaches to generating these embeddings are Long-Short-Term-Memories (LSTMs) and Convolutional Neural Networks (CNNs).
LSTMs can, in theory, have an unlimited context length $l$ but in practice, the context is truncated to the length of an average sentence or document.
We will focus on an LSTM implementation by \newcite{Graves13}.
This architecture is based on the work by \newcite{hochreiter1997long} which in turn is a modified version of a Recurrent Neural Network (RNN).
The implementation is based on several functions using the logistic sigmoid function:

\begin{equation} \eqlabel{sigmoid_function}
 \sigma(x) = \frac{1}{1+e^x}
\end{equation}

The input gate controls the information each cell lets in:

\begin{equation}
 i_t = \sigma(W_{xi} x_t + W_{hi} h_{t-1} + W_{ci} c_{t-1} + b_i) \eqlabel{LSTM_1}
\end{equation}

The forget gate controls the information each cell maintains over time:

\begin{equation}
 f_t = \sigma(W_{xf} x_t + W_{hf} h_{t-1} + W_{cf} c_{t-1} + b_f) \eqlabel{LSTM_2}
\end{equation}

After this the following function can be used to compute the cell state:

\begin{equation}
 c_t = f_t c_{t-1} + i_t \operatorname{tanh}(W_{xc} x_t + W_{hc} h_{t-1} + b_c) \eqlabel{LSTM_3}
\end{equation}

And finally an output gate controls the information that leaves the cell:

\begin{equation}
 o_t = \sigma(W_{xo} x_t + W_{ho} h_{t-1} + W_{co} c_t + b_o) \eqlabel{LSTM_4}
\end{equation}

All vectors are the same size as the hidden state vector $h$.
The next hidden state is computed by:

\begin{equation}
 h_t = o_t \operatorname{tanh}(c_t) \eqlabel{LSTM_5}
\end{equation}

These hidden state vectors $h_t$ are word-level embeddings where $x_t$ are normal word embeddings.
A sentence respectively a document is once read from left to right.
We call this the Forward LSTM with the corresponding hidden states $\overrightarrow{h_t}$.
We can also read the sentence, respectively document from right to left.
This is our Backward LSTM with hidden states $\overleftarrow{h_t}$.
The Bidirectional LSTM is defined by the concatenation of both:

\begin{equation} \eqlabel{bidir_concatenation}
h_t = (\overrightarrow{h_t}, \overleftarrow{h_t})
\end{equation}

So depending on our needs we can use word-level embeddings which incorporate the left context, the right context or both.

\subsection{Sentence Embeddings}
A sentence embedding encodes an entire sentence.
In our framework, we can use them to encode a question.
A simple approach is to use the word-level embedding of the first word concatenated with the word-level embedding of the last word.
Thus, a sentence embedding is given by:

\begin{equation} \eqlabel{question_embedding}
s = (\overrightarrow{h_T}, \overleftarrow{h_1})
\end{equation}

with $T$ being the sentence length.

Other more sophisticated strategies include encoder-decoder \cite{bahdanau2014neural} or seq2seq models \cite{sutskever2014sequence} to auto-encode the sentence.
Encoder and decoder are usually LSTMs (see previous subsection).
\newcite{kiros2015skip} try to mimic the Skip-gram model by predicting surrounding sentences, also using an encoder-decoder model.

\subsection{Document Embeddings}
Document embeddings must be able to either store a lot of information or to concentrate on the relevant information.
In theory, we can use the same technique we used for the sentence embedding for an entire document.
In practice, this does not work very well, as a document has too much information to encode it into a single vector.
We use an attention mechanism which uses the question to concentrate on the relevant information.
It can be seen as a forked neural network with the hidden state of the LSTM $h_t$ and the sentence encoding of the question $s_q$ as input:

\begin{equation}
 a^h_t = \sigma(W_{ha} h_t + W_{qa} s_q + b_a) \eqlabel{att_1}
\end{equation}

After the hidden layer $a^h_t$ follows we compute a scalar value $a^o_t$ as output:

\begin{equation}
 a^o_t = \sigma(W_{aa} a^h_t + b_{a'}) \eqlabel{att_2}
\end{equation}

A softmax function over the output of all hidden states gives us the final attention weight $a_t$:

\begin{equation}
 a_t   = \frac{\operatorname{exp}(a^o_t)}{\sum_{k=1}^T{\operatorname{exp}(a^o_k)}} \eqlabel{att_3}
\end{equation}

Instead of using a forked neural network with one hidden layer, we can also use a bilinear term to compute $a^o_t$:

\begin{equation}
 a^o_t = h_t^T W_{BL} s_q \eqlabel{att_bl}
\end{equation}

This method seems to be preferable if we assume $s_q$ and $h_t$ to live in a similar space \cite{luong2015effective}.
To get the document embedding we compute a weighted sum over all word-level embeddings:

\begin{equation} \eqlabel{document_embedding}
d = \sum_{t=1}^T a_t h_t
\end{equation}

We can now train a neural network, which minimizes the differences between the document embedding and the correct answer.
See \cite{nips15hermann} for more details.
By doing so we are able to learn document embeddings that capture the information relevant to the question.
The entire system is visualized in \figref{attentive_reader}.

\begin{figure}
\centering
\includegraphics[angle=-90,origin=c,width=0.85\textwidth]{images/attentive_reader}
\caption{An end-to-end system for question answering.}
\figlabel{attentive_reader}
\end{figure}

This is a fast-pacing field of research but LSTMs with attention mechanisms together with CNNs are the basis for an increasing number of state-of-the-art systems in Natural Language Processing.
While it is feasible to derive the gradient of the models we introduced in the beginning of the introduction, this might be cumbersome for an advanced deep neural network like \figref{attentive_reader}.
Libraries like Theano \cite{theano2016} or TensorFlow \cite{tensorflow2015-whitepaper} take over this task by automatic differentiation, i.e., applying the chain rule to all operation.

#!/usr/bin/env

import sys


def main(args):

    infile, outfile = args

    with open(infile) as f_in, open(outfile, 'w') as f_out:
        for line in f_in:
            tokens = line.split()
            if len(tokens) >= 2:
                type = tokens[0]
                name = tokens[1]
                descrp = ' '.join(tokens[2:])

                if type == 'G':

                    f_out.write(r"\newglossaryentry{%s}{name=%s, description={%s}}" % (name, name, descrp))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%s}{\texorpdfstring{\gls{%s}}{%s}\xspace}" % (name, name, name))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%sS}{\texorpdfstring{\glspl{%s}}{%s}\xspace}" % (name, name, name))
                    f_out.write("\n\n")

                else:
                    f_out.write(r"\newacronym{%s}{%s}{%s}" % (name, name, descrp))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%s}{\texorpdfstring{\gls{%s}}{%s}\xspace}" % (name, name, name))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%sS}{\texorpdfstring{\glspl{%s}}{%s}\xspace}" % (name, name, name))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%sFULL}{%s (\gls{%s})\xspace}" % (name, descrp, name))
                    f_out.write("\n")
                    f_out.write(r"\newcommand{\%sSFULL}{%s (\glspl{%s})\xspace}" % (name, descrp, name))
                    f_out.write("\n\n")

if __name__ == '__main__':
    main(sys.argv[1:])
